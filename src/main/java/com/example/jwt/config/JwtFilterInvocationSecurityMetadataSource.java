package com.example.jwt.config;

import com.example.jwt.dao.MenuMapper;
import com.example.jwt.entity.Menu;
import com.example.jwt.entity.Role;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

/**
 * 获取访问当前资源所需要的角色
 */
@Component
public class JwtFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    @Resource
    private MenuMapper menuMapper;

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        // 获取要访问的资源
        String requestUrl = ((FilterInvocation) object).getRequestUrl();
        List<Menu> allMenu = menuMapper.selectMenuListWithRoles();

        allMenu.sort((m1, m2) -> m2.getMenuUrl().length() - m1.getMenuUrl().length());
        // 注意这里遍历 menu 时要注意 menu 的顺序, 如 /ry 和 /ry/view 都可以被 /ry/** 匹配到, 但是他们可能接受的用户角色不同
        // 可以选择数据库查出 menu 时按照 menuUrl 的长度进行排序
        for (Menu menu : allMenu) {
            List<Role> roles = menu.getRoles();
            if (antPathMatcher.match(menu.getMenuUrl(), requestUrl)) {
                if (roles.isEmpty()) {
                    // 如果匹配到路径, 但是没有配置角色, 则任何人都可以访问
                    return SecurityConfig.createList();
                }
                String[] roleIds = roles.stream().map(Role::getRoleName).toArray(String[]::new);
                return SecurityConfig.createList(roleIds);
            }
        }

        // 如果没有匹配到的路径, 或者该路径没有配置角色, 则说明这个路径任何角色都可以访问
        return SecurityConfig.createList();
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return FilterInvocation.class.isAssignableFrom(clazz);
    }
}
