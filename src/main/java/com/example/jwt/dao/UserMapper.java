package com.example.jwt.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jwt.entity.JwtUserDetails;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserMapper extends BaseMapper<JwtUserDetails> {

    List<JwtUserDetails> selectUserListWithRoles();

    JwtUserDetails selectUserWithRolesByUsername(@Param("username") String username);

    JwtUserDetails selectUserWithRolesById();

    @MapKey("id")
    Map<Long, JwtUserDetails> selectMap();
}
