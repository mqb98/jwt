package com.example.jwt.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jwt.entity.Menu;

import java.util.List;

public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> selectMenuListWithRoles();

}
