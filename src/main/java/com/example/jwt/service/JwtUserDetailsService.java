package com.example.jwt.service;

import com.example.jwt.dao.UserMapper;
import com.example.jwt.entity.JwtUserDetails;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Resource
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        JwtUserDetails jwtUserDetails = userMapper.selectUserWithRolesByUsername(s);
        if (jwtUserDetails == null) {
            throw new UsernameNotFoundException("该用户名不存在");
        }
        return jwtUserDetails;
    }
}
