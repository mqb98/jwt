package com.example.jwt.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
@TableName(value = "tb_menu")
public class Menu {
    private Long id;
    private Long menuId;
    private String menuName;
    private Long parentId;
    private String menuUrl;
    private String menuPath;
    private Boolean menuEnabled;
    private List<Role> roles;
}
