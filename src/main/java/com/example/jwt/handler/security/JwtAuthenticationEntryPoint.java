package com.example.jwt.handler.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 用于匿名用户访问被拒绝时的处理
 * 当匿名无法访问时, 在 JwtUrlAccessDecisionManager 抛出一个 AccessDeniedException,
 * 这个异常会被 AbstractSecurityInterceptor#beforeInvocation 捕获到, 然后将这个异常作为事件发布, 再将之抛出.
 * 这里再次抛出会被 ExceptionTranslationFilter#doFilter 捕获到, 后面回来到方法 handleSpringSecurityException 来处理异常,
 * 如果当前的 Authentication 属于 匿名 或者 RememberMe, 则会调用 sendStartAuthentication 方法来处理,
 * 在这个方法中, 调用了  authenticationEntryPoint.commence
 * @author maqb
 */
@Component
@Slf4j
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        log.debug("{}", authException.getMessage());
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setHeader("Content-Type", "application/json;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.write("请先登陆!");
        out.flush();
    }
}
