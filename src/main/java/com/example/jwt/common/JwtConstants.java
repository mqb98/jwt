package com.example.jwt.common;

public class JwtConstants {

    /**
     * 从请求头的 Authorization 属性获取 Token
     */
    public final static String JWT_TOKEN_REQUEST_HEAD = "Authorization";

    public final static String RESPONSE_TYPE = "application/json;charset=utf-8";

}
