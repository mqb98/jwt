/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80024
 Source Host           : localhost:3306
 Source Schema         : maqb_auth

 Target Server Type    : MySQL
 Target Server Version : 80024
 File Encoding         : 65001

 Date: 19/05/2021 15:28:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu`;
CREATE TABLE `tb_menu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `menu_id` int(0) NOT NULL,
  `menu_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parent_id` int(0) NOT NULL DEFAULT 0,
  `menu_url` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `menu_path` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `menu_enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_menu
-- ----------------------------
INSERT INTO `tb_menu` VALUES (1, 1, '部门管理', 0, '/bm/**', NULL, 1);
INSERT INTO `tb_menu` VALUES (2, 2, '人员管理', 0, '/ry/**', NULL, 1);
INSERT INTO `tb_menu` VALUES (3, 3, '人员查看', 0, '/ry/view/**', NULL, 1);

-- ----------------------------
-- Table structure for tb_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu_role`;
CREATE TABLE `tb_menu_role`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `menu_id` int(0) NOT NULL,
  `role_id` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_menu_role
-- ----------------------------
INSERT INTO `tb_menu_role` VALUES (1, 1, 1);
INSERT INTO `tb_menu_role` VALUES (2, 2, 2);
INSERT INTO `tb_menu_role` VALUES (3, 2, 3);

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `role_id` int(0) NOT NULL,
  `role_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `zh_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色中文名字',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES (1, 1, 'ROLE_boss', '老板');
INSERT INTO `tb_role` VALUES (2, 2, 'ROLE_manager', '经理');
INSERT INTO `tb_role` VALUES (3, 3, 'RLE_director', '总监');
INSERT INTO `tb_role` VALUES (4, 4, 'ROLE_staff', '员工');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username_unique`(`username`) USING BTREE COMMENT 'username 不可重复'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'spencer', '$2a$10$y9T03Zp1BCiA2U6w0UfEkeekXuSDbjGvpG/dqNrllj4xH7O5a28Ta', 1);
INSERT INTO `tb_user` VALUES (2, 'maqb', '$2a$10$vrB.kZ7d86gzWOI5PD3K6e7CqS.MK6D2u60EbQMZdgQ45cN/BiKGy', 1);
INSERT INTO `tb_user` VALUES (3, 'huff', '$2a$10$Q81Om4d6zu25F7fz14/OJ.4rkLMun3PwK2y9wAAkUcO.VJWavQeDm', 1);
INSERT INTO `tb_user` VALUES (4, 'lr1', '$2a$10$grV8.1fCHl5926fQLZr5ouVl6aUG5/CZWl6gF8FHHRGWvhVl2YL9W', 1);
INSERT INTO `tb_user` VALUES (5, 'lr2', '$2a$10$grV8.1fCHl5926fQLZr5ouVl6aUG5/CZWl6gF8FHHRGWvhVl2YL9W', 1);
INSERT INTO `tb_user` VALUES (6, 'lr3', '$2a$10$grV8.1fCHl5926fQLZr5ouVl6aUG5/CZWl6gF8FHHRGWvhVl2YL9W', 1);

-- ----------------------------
-- Table structure for tb_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_role`;
CREATE TABLE `tb_user_role`  (
  `id` int(0) NOT NULL,
  `user_id` int(0) NOT NULL,
  `role_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user_role
-- ----------------------------
INSERT INTO `tb_user_role` VALUES (1, 1, 1);
INSERT INTO `tb_user_role` VALUES (2, 2, 2);
INSERT INTO `tb_user_role` VALUES (3, 3, 3);
INSERT INTO `tb_user_role` VALUES (4, 4, 4);
INSERT INTO `tb_user_role` VALUES (5, 5, 4);
INSERT INTO `tb_user_role` VALUES (6, 6, 4);

SET FOREIGN_KEY_CHECKS = 1;
