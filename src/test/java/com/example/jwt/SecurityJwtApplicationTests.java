package com.example.jwt;

import com.example.jwt.dao.MenuMapper;
import com.example.jwt.dao.UserMapper;
import com.example.jwt.entity.JwtUserDetails;
import com.example.jwt.entity.Menu;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@SpringBootTest
@Slf4j
class SecurityJwtApplicationTests {

    @Resource
    UserMapper userMapper;

    @Resource
    MenuMapper menuMapper;

    @Test
    void contextLoads() {
//        List<JwtUserDetails> jwtUserDetails =
//                userMapper.selectUserListWithRoles();
//        jwtUserDetails.forEach(System.out::println);
//
//        System.out.println("--------------------------");
//        List<Menu> menus = menuMapper.selectMenuListWithRoles();
//        menus.forEach(System.out::println);

        Map<Long, JwtUserDetails> map = userMapper.selectMap();

        map.forEach((s, jwtUserDetails) -> log.debug("key -> {}, value -> {}", s, jwtUserDetails));
    }

    @Test
    void t1() {
        List<ConfigAttribute> list = SecurityConfig.createList("admin", "user");
        System.out.println(list);
    }

}
